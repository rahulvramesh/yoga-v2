import {Contact} from "./contact";

export const CONTACTS: Contact[] = [
	{
		userId: "USR001",
		foodName: "Apple",
		qty: "10",
		time: "Morning",
		notes: "Eat On Time"
	}
]
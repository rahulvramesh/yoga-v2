import {Component,OnInit} from 'angular2/core';
import * as Rx from 'rxjs/Rx';
import {Router}               from 'angular2/router';
import {ContactService} from "./contact.service";
import {Contact} from "./contact";


@Component({
    selector: 'level-one',
    templateUrl: './app/wizard/levelone.component.html',
    host: { 'class': 'ng-animate page1Container' },
    styleUrls: ['./assets/topbar.css'],
    providers: [ContactService]
})
export class LeveloneComponent implements OnInit { 
   	
   	public contacts: Contact[];
	date = new Date();

	newPackage: Contact;

	constructor(private _contactService: ContactService, private _router: Router) {
		this.getContacts();
		this.newPackage = { userId: '', foodName: '', time: '', notes: '', qty: '' };
	}

	getContacts() {
		this._contactService.getContacts().then((contacts: Contact[]) => this.contacts = contacts);
	}

	ngOnInit(): any {
		this.getContacts();
		console.log(this.contacts);
	}

	onAddItem(){
		console.log(this.newPackage);
		this.newPackage.userId = "USR" + Math.floor((Math.random() * 100) + 1).toString();
		this._contactService.insertContact(this.newPackage);
		this.newPackage = { userId: '', foodName: '', time: '', notes: '', qty: '' };
		this._router.navigate(['LevelOne']);
	}
	

}

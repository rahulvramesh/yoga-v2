export interface Contact {
  userId :string,
  foodName: string,
  qty : string,
  time: string,
  notes: string
}
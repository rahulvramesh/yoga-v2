import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {TopbarComponent} from './topbar/topbar.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LeveloneComponent} from './wizard/levelone.component';

@Component({
  selector: 'myapp',
  template: `

  <top-bar></top-bar>
  <!-- <dashboard></dashboard>-->

  <router-outlet></router-outlet>

  `,
  directives:[TopbarComponent, DashboardComponent, ROUTER_DIRECTIVES]
})

  @RouteConfig([
    { path: '/dashboard', name: 'Dashboard', component: DashboardComponent,useAsDefault:true },
    { path: '/levelone', name: 'LevelOne', component: LeveloneComponent },
  ])

export class AppComponent {}
import {Component} from 'angular2/core';

import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router'; 
import {ROUTER_DIRECTIVES, RouteConfig, Router, Location, Route} from 'angular2/router';

@Component({
	selector: 'top-bar',
	templateUrl:'./app/topbar/topbar.component.html',
	styleUrls: ['./assets/topbar.css'],
	directives: [ROUTER_DIRECTIVES]
})
export class TopbarComponent {
	router: Router; 
	location: Location; 
	constructor(router: Router, location: Location) { 
		this.router = router; this.location = location; 
	} 
	getLinkStyle(path) { 
		return this.location.path() === path; 
	}

}